using System.IdentityModel.Tokens.Jwt;
using SiteUser.DataLayer.Models;

namespace SiteUser.BusinessLogicLayer.TokenBLL;

public interface ITokenService
{ 
        JwtSecurityToken CreateTokenOptions(User user); 
        JwtSecurityToken CreateTokenOption(); 
}