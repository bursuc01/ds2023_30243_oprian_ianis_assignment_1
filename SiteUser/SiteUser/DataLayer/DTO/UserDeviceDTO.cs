namespace SiteUser.DataLayer.DTO;

public class UserDeviceDTO
{
    public int Id { get; set; }
    public UserDeviceDTO(int id)
    {
        Id = id;
    }
}