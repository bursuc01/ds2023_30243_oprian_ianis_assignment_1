namespace SiteUser.DataLayer.DTO;

public class UserLoginDTO
{
    public string? Name { get; set; }
    public string? Password { get; set; }
}