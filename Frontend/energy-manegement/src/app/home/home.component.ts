import { Component } from '@angular/core';
import { DeviceService } from '../services/device-service/device.service';
import { UserService } from '../services/user-service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  deviceList: any[] = [];
  username: string = '';

  constructor(
    private deviceService: DeviceService,
    private userService: UserService
  ) {}

  ngOnInit() {
    const user = this.userService.getUserFromToken();
    this.username = user.name;
    this.deviceService.getUserDeviceList(user.id).subscribe(
      (data) => {
        this.deviceList = data;
        console.log(this.deviceList);
      }
    );
  }
}
