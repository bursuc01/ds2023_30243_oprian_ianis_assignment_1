import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Device } from 'src/app/interfaces/device';
import { DeviceCreate } from 'src/app/interfaces/deviceCreate';
@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  private linkUrl = 'http://localhost:5267/api/Device';
  private userUrl = 'http://localhost:5267/api/User';
  constructor(
    private http: HttpClient
  ) { }

  getUserDeviceList(userId: number): Observable<any> {
    let getUrl = this.userUrl + '?id=' + userId;
    return this.http.get(getUrl);
  }

  postDevice(device: DeviceCreate): Observable<any> {
    return this.http.post<Device>(this.linkUrl, device);
  }

  deleteDevice(deviceId: number): Observable<any> {
    const deleteUserUrl = this.linkUrl + '/' + deviceId;
    return this.http.delete<any>(deleteUserUrl);
  }

  linkDeviceToUser(userId: number, deviceId: number): Observable<any> {
    const updateUrl = this.userUrl + '/link?deviceId=' + deviceId + '&userId=' + userId;
    return this.http.put<any>(updateUrl,[]);
  }

  unlinkDeviceFromUser(userId: number, deviceId: number): Observable<any> {
    const updateUrl = this.userUrl + '/unlink?deviceId=' + deviceId + '&userId=' + userId;
    return this.http.put<any>(updateUrl,[]);
  }
  
  updateDevice(updatedDeviceData: any): Observable<any> {
    return this.http.put<any>(this.linkUrl, updatedDeviceData);
  }

  getDevices(): Observable<any[]> {
    return this.http.get<any[]>(this.linkUrl);
  }
}
