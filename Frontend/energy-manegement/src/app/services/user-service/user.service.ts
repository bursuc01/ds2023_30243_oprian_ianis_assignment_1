import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { AuthenticatedResponse } from 'src/app/interfaces/authenticated-response';
import { Router } from '@angular/router';
import { Login } from 'src/app/interfaces/login';
import { Observable } from 'rxjs';
import { UserCreate } from 'src/app/interfaces/userCreate';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersUrl = 'https://localhost:7006/api/User';  // URL to web api
  private loginUrl: string = "https://localhost:7006/api/Authenticate/login";
  public isLoggedIn: boolean = false;
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  // Requests
  login(credentials: Login): Observable<AuthenticatedResponse> {
    return this.http.post<AuthenticatedResponse>(this.loginUrl, credentials, this.httpOptions);
  }

  
  // Utilities
  saveToken(token: string) {
    this.saveToLocalStorage("jwt", token);
    const savedUser = this.getUserFromToken();
    console.log(savedUser);
    if(savedUser.isAdmin) {
      console.log("admin");
      this.router.navigate(["/admin"]);
    }
    else{
      console.log("user");
      this.router.navigate(["/home"]);
    }
  }

  getUsers(): Observable<any[]> {
    return this.http.get<any[]>(this.usersUrl);
  }

  postUser(user: UserCreate): Observable<any> {
    return this.http.post<User>(this.usersUrl, user);
  }

  deleteUser(userId: number): Observable<any> {
      const deleteUserUrl = this.usersUrl + '/' + userId;
      return this.http.delete<any>(deleteUserUrl);
  }

  updateUser(updatedUserData: any): Observable<any> {
    return this.http.put<any>(this.usersUrl, updatedUserData);
  }

  saveToLocalStorage(tag: string, content: any) {
    localStorage.setItem(tag, content);
  }

  getUserFromToken(): User {
    const token = localStorage.getItem("jwt");
    if (token) {
      const tokenParts = token.split('.');
      if (tokenParts.length === 3) {
        const payload = JSON.parse(atob(tokenParts[1]));
        
        const isAdmin = payload.IsAdmin.toLowerCase() === 'true';
        
        return { id: payload.Id, name: payload.Name, isAdmin: isAdmin };
      }
    }
  
    return { id: 0, name: '', isAdmin: false };
  }
  

  isUserAuthenticated = (): boolean => {
    return (localStorage.getItem("jwt") != null);
  }

  isAdmin = (): boolean => {
    const user = this.getUserFromToken();
    return user.isAdmin;
  }
}
