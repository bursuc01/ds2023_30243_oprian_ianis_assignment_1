export interface DeviceCreate {
    description: string,
    address: string,
    maximumHourlyEnergyConsumption: number
  }