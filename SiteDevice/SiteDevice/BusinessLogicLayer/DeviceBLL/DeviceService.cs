using AutoMapper;
using SiteDevice.DataLayer.DTO;
using SiteDevice.DataLayer.Models;
using SiteDevice.DataLayer.Repository.DeviceRepository;

namespace SiteDevice.BusinessLogicLayer.DeviceBLL;

public class DeviceService : IDeviceService
{
    private readonly IDeviceRepository _deviceRepository;
    private readonly IMapper _mapper;

    public DeviceService(
        IDeviceRepository deviceRepository,
        IMapper mapper)
    {
        _deviceRepository = deviceRepository;
        _mapper = mapper;
    }

    public async Task<IEnumerable<DeviceGetDTO>> GetDevicesAsync()
    {
        var devices = await _deviceRepository.GetDevicesAsync();
        return _mapper.Map<IEnumerable<DeviceGetDTO>>(devices);
    }

    public async Task<DeviceDTO> GetDeviceAsync(int id)
    {
        var device = await _deviceRepository.GetDeviceAsync(id);
        return _mapper.Map<DeviceDTO>(device);
    }

    public async Task PutDeviceAsync(DeviceGetDTO device)
    {
        var actualDevice = _mapper.Map<Device>(device);
        await _deviceRepository.PutDeviceAsync(actualDevice);
    }

    public async Task PostDeviceAsync(DeviceDTO device)
    {
        var actualDevice = _mapper.Map<Device>(device);
        await _deviceRepository.PostDeviceAsync(actualDevice);
    }

    public async Task DeleteDeviceAsync(int id)
    {
        await _deviceRepository.DeleteDeviceAsync(id);
    }
}